CREATE DATABASE IF NOT EXISTS cuentavotos CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci';
GRANT ALL PRIVILEGES ON cuentavotos.* TO 'cuentavotos'@'localhost' IDENTIFIED BY 'votosdb';
FLUSH PRIVILEGES;
