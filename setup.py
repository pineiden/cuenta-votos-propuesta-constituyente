from setuptools import setup
from pathlib import Path
import os 
from setuptools.command.install import install

path = Path(__file__).resolve().parent
with open(path/'README.md', encoding='utf-8') as f:
    long_description = f.read()

with open(path/'VERSION') as version_file:
    version = version_file.read().strip()



REQUIREMENTS_FILE = 'requirements.txt'
DB = os.environ.get("DATABASE_URL", None)
if DB:
    REQUIREMENTS_FILE = 'requirements_mariadb.txt'

with open(path/REQUIREMENTS_FILE, encoding='utf-8') as req_file:
    REQUIREMENTS = req_file.read().strip()


def save_env(path=__file__):
    this_path = Path(path).absolute().parent
    if path != __file__:
        this_path = Path(path).absolute()
    if not this_path.exists():
        path.mkdir(parents=True, exist_ok=True)
    SQLITE =f"sqlite:///{this_path}/cuentavotos.sqlite"
    db_url = os.environ.get("DATABASE_URL", SQLITE)
    with open(this_path /".env", 'w') as f:
        f.write(f"DATABASE_URL={db_url}\n")
        if os.name == 'nt:':
            log_path =f"C:\\log_votos" 
            Path(log_path).mkdir(parents=True, exist_ok=True)
            f.write(f"LOG_PATH={log_path}\n")        
        else:
            log_path =f"/tmp/log/votos"
            Path(log_path).mkdir(parents=True, exist_ok=True)
            f.write(f"LOG_PATH={log_path}\n")
    with open(this_path /".env","r") as f:
        txt = f.read()
        return this_path, txt
    
import sys 

class PostInstallCommand(install):
    """Post-installation for installation mode."""
    def run(self):
        def find_module_path():
            for p in sys.path:
                if os.path.isdir(p) and "cuentavotos" in os.listdir(p):
                    return os.path.join(p, "cuentavotos")
        install_path = find_module_path()
        print("Installpath", install_path)
        result  = save_env(install_path)
        install.run(self)

s = setup(name='cuentavotos',
      version=version,
      description='Monitor de votaciones de norma constituyente',
      url='https://gitlab.com/pineiden/cuenta-votos-propuesta-constituyente',
      author='David Pineda Osorio',
      author_email='https://gitlab.com/pineiden/cuenta-votos-propuesta-constituyente',
      license='GPLv3',
      packages=['cuentavotos'],
      keywords = ["collector", "gnss", "scheduler", "async", "multiprocess"],
      install_requires=REQUIREMENTS,
      entry_points={
        'console_scripts':[
            "cuentavotos = cuentavotos.extractor:run",
            "crear_db = cuentavotos.database.create_db:run ",
            "cargar_normas = cuentavotos.database.load_normas:run ",
            "listar_normas = cuentavotos.database.list_normas:run ",
            "otras_normas = cuentavotos.database.otras_normas:run ",
            "cargar_votos = cuentavotos.database.search_votos:run ",
            "listar_votos = cuentavotos.database.listar_votos:run ",
            "ultimo_voto = cuentavotos.database.ultimo_voto:run ",
            "graficar = cuentavotos.database.grafica_data:run ",
        ]
        },
      cmdclass={
          "install": PostInstallCommand
      },
      include_package_data=True,
      long_description=long_description,
      long_description_content_type='text/markdown',
      zip_safe=False)
