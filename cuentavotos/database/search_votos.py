import csv 
import click 
from rich import print 
from pathlib import Path 
from rich.table import Table
from rich.console import Console
from slugify import slugify
from .manager import SessionDB
import os
from datetime import datetime

@click.command()
@click.option("--base_path", help="Directorio dónde buscar")
@click.option("--load/--no-load", default=False, help="Directorio dónde buscar")

def run(base_path, load):
    console = Console()
    table = Table(title="Nuevas normas a monitorear")
    table.add_column("id", style="cyan")
    table.add_column("titulo", style="red")
    table.add_column("archivo", style="blue")
    table.add_column("tamaño (bytes)", style="green")
    table.add_column("datos", style="cyan")

    session = SessionDB().connect()
    for norma in session.normas.values():
        path = Path(base_path)
        if path.exists():
            slug = slugify(norma.titulo)
            for filepath in path.glob(f"{slug}*.csv"):
                size = Path(filepath).stat().st_size
                if load:
                    counter = 0
                    with open(filepath, "r") as f:
                        reader = csv.DictReader(f, delimiter=";")
                        for row in reader:
                            kwargs = {
                                "norma_id": norma.id,
                                "dt_gen": datetime.fromisoformat(row.get("fecha-hora")),
                                "cantidad": row.get("votos")
                            }
                            voto, nuevo = session.voto(**kwargs)
                            if nuevo:
                                counter += 1
                    table.add_row(
                        str(norma.id),
                        norma.titulo, 
                        str(filepath), 
                        str(size), str(counter))           
                else:
                    table.add_row(
                        str(norma.id),
                        norma.titulo, 
                        str(filepath), 
                        str(size), str(0))           
    console.print(table)

if __name__ == '__main__':
    run()
