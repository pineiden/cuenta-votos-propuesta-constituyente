import csv 
import click 
from rich import print 
from pathlib import Path 
from rich.table import Table
from rich.console import Console
from slugify import slugify
from .manager import SessionDB
import os
from datetime import datetime, timedelta

"""
Obtener ultimo voto, leer tabla de normas desde archivo, o tomar de opcion
"""

def check_norma(                        
        session, 
        id_norma, 
        table,
        grupo_normas,
        dias,
        checked, 
        filewriter=None,
        writer=None):
    norma_instance = grupo_normas.get(id_norma)
    if not norma_instance:           
        grupo_normas[id_norma] = session.get_norma(id_norma)
        norma_instance = grupo_normas[id_norma]
    if norma_instance and id_norma not in checked:
        now = datetime.utcnow()
        hace_un_dia = now - timedelta(days=dias)
        voto = session.ultimo_voto(norma_instance, end_date=now)
        voto_hace_un_dia = session.ultimo_voto(norma_instance,
                                               end_date=hace_un_dia)
        diferencia = 0
        if voto_hace_un_dia:
            diferencia = voto.get("cantidad") - voto_hace_un_dia.get("cantidad")
        table.add_row(
            str(norma_instance.id),
            norma_instance.titulo,
            str(voto.get("id")),
            voto.get("dt_gen").isoformat(),
            str(voto.get("cantidad")),
            str(diferencia)
        )
        checked.add(id_norma)
        if filewriter:
            voto["diferencia"] = diferencia
            writer.writerow(voto)
    else:
        print(f"La norma {id_norma} no existe o ya fue registrada")


@click.command()
@click.option("--normas", "-n",default=[0], help="Id(s) de normas dónde buscar", multiple=True)
@click.option("--grupo", "-g",default="normas.csv", help="Archivo de"+
              "normas con id, titulo")
@click.option("--dias", "-d",default=1, help="Cantidad de Dias diferencia")
@click.option("--titulo", "-t" ,default="Normas Populares", help="Título del grupo de normas")
@click.option("--guardar/--no-guardar", default=False, help="Directorio dónde buscar")
@click.option("--filename", default="reporte.csv", help="Archivo a guardar")
def run(normas, grupo, dias, titulo, guardar, filename):
    console = Console()
    table = Table(title=f"Votos para norma {titulo},"+
                  f" diferencia {dias} dias")
    table.add_column("norma id", style="cyan")
    table.add_column("titulo", style="red")
    table.add_column("id", style="cyan")
    table.add_column("fecha-hora", style="blue")
    table.add_column("cantidad", style="green")
    table.add_column("diferencia", style="green")
    session = SessionDB().connect()
    filewriter = None
    writer = None
    if guardar:
        print("Filename", filename)
        filewriter = open(filename,"w")
        fileheaders = [
            "id",
            "norma_id", 
            "titulo", 
            "dt_gen", 
            "cantidad", 
            "diferencia"]
        writer = csv.DictWriter(
            filewriter, 
            delimiter=";", 
            fieldnames=fileheaders,
            quoting=csv.QUOTE_MINIMAL)
        writer.writeheader()
    grupo_normas = {}
    checked = set()
    # check list
    for norma_id in normas:
        check_norma(
         session, 
         norma_id, 
         table,
         grupo_normas, 
         dias,
         checked, 
         filewriter, 
         writer)                   
    # check file
    if Path(grupo).exists():
        with open(grupo, "r") as f:
            reader = csv.DictReader(f, delimiter=";")
            for row in reader:
                norma_id = int(row.get("id"))
                if norma_id:
                       check_norma(
                        session, 
                        norma_id, 
                        table,
                        grupo_normas, 
                        dias,
                        checked, 
                        filewriter, 
                        writer)                   
    console.print(table)
    if filewriter:
        filewriter.close()

if __name__ == '__main__':
    run()
