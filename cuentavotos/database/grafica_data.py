import csv
import click
from rich import print
from pathlib import Path
from rich.table import Table
from rich.console import Console
from slugify import slugify
from .manager import SessionDB
import os
from datetime import datetime
from .graficar_votacion import PlotNormas, PlotNormasCSVSource

import pytz

tz = pytz.timezone("America/Santiago")

@click.command()
@click.option("--normas", "-n", default=[0], help="Id(s) de normas dónde buscar", multiple=True)
@click.option("--start", default="2021-11-01T00:00:00", help="Fecha de partida")
@click.option("--end", default="2022-02-01T00:00:00", help="Fecha de final")
@click.option("--path", default="datos.csv", help="Directorio dónde buscar")
@click.option("--csv/--no-csv", default=False, help="Directorio dónde" +
              "buscar")
@click.option("--csv/--no-csv", default=False, help="Directorio dónde buscar")
@click.option("--plot", default="datos_normas.png", help="Directorio" +
              "para guardar gráfica")
def run(normas, start, end, path, csv, plot):
    console = Console()
    table = Table(title="Graficas de normas")
    table.add_column("id", style="cyan")
    table.add_column("titulo", style="red")

    session = SessionDB().connect()
    plot_data = None
    if csv:
        plot_data = PlotNormasCSVSource(normas, start, end, path)
    else:
        dataset = []
        start_date = datetime.fromisoformat(start).astimezone(tz)
        end_date = datetime.fromisoformat(end).astimezone(tz)

        for id_norma in normas:
            norma_instance = session.get_norma(id_norma)
            table.add_row(str(id_norma), norma_instance.titulo)
            if norma_instance:
                this_dataset = session.get_votos(norma_instance,
                                                 start_date, end_date)
            dataset += this_dataset
        if len(dataset)>1:
            plot_data = PlotNormas(normas, start, end, dataset)
        else:
            print(f"En este tramo no hay mas de un dato para la(s) norma(s)" 
                  + f"seleccionada(s): {normas}")
    if plot_data:
        print("Mostrando plots")
        plot_data.show()
        plot_data.save(filename=plot)
    console.print(table)


if __name__ == '__main__':
    run()
