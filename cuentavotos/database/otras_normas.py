import csv 
import click 
from rich import print 
from pathlib import Path 
from rich.table import Table
from rich.console import Console
from slugify import slugify
from .manager import SessionDB
import os
from datetime import datetime, timedelta


@click.command()
@click.option("--normas", "-n",
              default=[0], 
              help="Id(s) de normas para"+
              "buscar las otras", 
              multiple=True)
def run(normas):
    console = Console()
    table = Table(title=f"Otras normas a las listadas")
    table.add_column("norma id", style="cyan")
    table.add_column("titulo", style="red")
    session = SessionDB().connect()
    filewriter = None
    writer = None
    grupo_normas = {}
    checked = set()
    # check list
    otras_normas = session.get_new_normas(normas)
    for norma in otras_normas:
        table.add_row(
            str(norma.id),
            norma.titulo,
        )

    console.print(table)
if __name__ == '__main__':
    run()
