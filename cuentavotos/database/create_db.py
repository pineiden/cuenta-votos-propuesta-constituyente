from sqlalchemy import create_engine
from sqlalchemy.schema import CreateSchema
from sqlalchemy import event
from sqlalchemy.sql import exists, select
from .models import Base
import click
from rich import print
from dotenv import dotenv_values
from pathlib import Path 

@click.command()
def run():
    path = Path(__file__).absolute().parent.parent / ".env"
    if not path.exists():
        path = Path(__file__).absolute().parent.parent.parent / ".env"
    print("PATH .ENV", path)
    params = dotenv_values(path)
    DB_URL = params.get("DATABASE_URL")
    db_engine = DB_URL
    #create engine
    engine = create_engine(
        db_engine,
        echo=True)
    print(db_engine)
    #load schema on engine
    try:
        step = "Crear database"
        #engine.execute(CreateSchema('constitucion'))
        step = "Crear scheme"
        Base.metadata.create_all(engine, checkfirst=True)
    except Exception as e:
        print(f"Falla al crear esquema de tablas {step}")
        raise e


if __name__ == '__main__':
    run()
