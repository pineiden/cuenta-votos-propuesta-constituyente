import csv 
import click 
from rich import print 
from pathlib import Path 
from rich.table import Table
from rich.console import Console

from .manager import SessionDB

@click.command()
def run():
    console = Console()
    table = Table(title="Nuevas normas a monitorear")
    table.add_column("id", style="cyan")
    table.add_column("titulo", style="red")
    session = SessionDB().connect()
    for norma in session.normas.values():
        table.add_row(
            str(norma.id),
            norma.titulo)
    console.print(table)

if __name__ == '__main__':
    run()
