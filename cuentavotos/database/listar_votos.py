import csv 
import click 
from rich import print 
from pathlib import Path 
from rich.table import Table
from rich.console import Console
from slugify import slugify
from .manager import SessionDB
import os
from datetime import datetime



@click.command()
@click.option("--normas", "-n",default=[0], help="Id(s) de normas dónde buscar", multiple=True)
@click.option("--start", default="2021-11-01T00:00:00", help="Fecha de partida")
@click.option("--end", default="2022-02-01T00:00:00", help="Fecha de final")
@click.option("--guardar/--no-guardar", default=False, help="Directorio dónde buscar")
@click.option("--filename", default="datos.csv", help="Archivo a guardar")
def run(normas, start, end, guardar, filename):
    console = Console()
    table = Table(title=f"Últimos Votos para norma {normas}, fechas ({start},{end})")
    table.add_column("id", style="cyan")
    table.add_column("norma id", style="cyan")
    table.add_column("titulo", style="red")
    table.add_column("fecha-hora", style="blue")
    table.add_column("cantidad", style="green")
    table.add_column("delta <H : M : S>", style="orange3")
    table.add_column("tasa <votos/minuto>", style="green")
    session = SessionDB().connect()
    filewriter = None
    if guardar:
        filewriter = open(filename,"w")
        fileheaders = ["id","norma_id", "titulo", "dt_gen", "cantidad", "delta", "tasa"]
        writer = csv.DictWriter(filewriter, delimiter=";", fieldnames=fileheaders,
                                quoting=csv.QUOTE_MINIMAL)

    for id_norma in normas:
        norma_instance = session.get_norma(id_norma)
        if norma_instance:
            start_date = datetime.fromisoformat(start)
            end_date = datetime.fromisoformat(end)
            for voto in session.get_votos(norma_instance, start_date,
                                          end_date):
                table.add_row(str(voto.get("id")), str(norma_instance.id),norma_instance.titulo, voto.get("dt_gen").isoformat(),
                              str(voto.get("cantidad")), str(voto.get("delta")), str(voto.get("tasa")))
                anterior = voto
                if filewriter:
                    writer.writerow(voto)
            console.print(table)
        else:
            print(f"La norma {id_norma} no existe")
    if filewriter:
        filewriter.close()

if __name__ == '__main__':
    run()
