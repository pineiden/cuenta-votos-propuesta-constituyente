from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from dotenv import load_dotenv
from dotenv import dotenv_values
from pathlib import Path
import databases
import os

class DBSession:
    
    def __init__(self, *args, **kwargs):
        path = Path(__file__).absolute().parent.parent / ".env"
        if not path.exists():
            path = Path(__file__).absolute().parent.parent.parent / ".env"            
        params = dotenv_values(path)
        DB_URL = params.get("DATABASE_URL")
        if not DB_URL:
            DB_URL = os.environ.get("DATABASE_URL")
        self.is_sqlite = False
        if "sqlite" in DB_URL:
            self.is_sqlite = True
        self.db_engine = DB_URL
        self.data = kwargs
        self.connection = None
        
    def get_session(self):
        return self.session

    def connect(self):
        if self.is_sqlite:
            self.engine = create_engine(
                self.db_engine, 
                connect_args={"check_same_thread": False})
        else:
            self.engine = create_engine(self.db_engine)        
        self.connection = self.engine.connect()
        self.session = sessionmaker(
            autocommit=False,
            autoflush=False,
            bind=self.engine)()
        return self

    async def async_connect(self):
        self.database = databases.Database(self.db_engine)
        self.engine = create_engine(self.db_engine)
        self.connection = await self.database.connect()
        self.session = sessionmaker(bind=self.engine)()

    async def async_disconnect(self):
        await self.database.disconnect()
        self.connection = None
        
    def run_sql(self, sql):
        self.connection.execute(sql)

        
    def close(self):
        try:
            self.session.close()
            self.connection.close()
        except Exception as e:
            print("Error on close",e)
            raise e
