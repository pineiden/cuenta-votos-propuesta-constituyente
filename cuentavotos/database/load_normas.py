import csv 
import click 
from rich import print 
from pathlib import Path 
from rich.table import Table
from rich.console import Console

from .manager import SessionDB

@click.command()
@click.option("--normas", help="Entregar ruta a lista de normas (numero,titulo)")
@click.option("--separador", default=";", help="Entregar ruta a lista de normas (numero,titulo)")
def run(normas, separador):
    console = Console()
    if normas:
        if Path(normas).exists():
            table = Table(title="Nuevas normas a monitorear")
            table.add_column("id", style="cyan")
            table.add_column("titulo", style="red")
            table.add_column("nueva", style="blue")
            session = SessionDB().connect()
            with open(normas) as f:
                reader = csv.DictReader(f, delimiter=separador,
                                        quoting=csv.QUOTE_MINIMAL)
                
                for i,line in enumerate(reader):
                    line = {key:line.get(key) for key in {"id","titulo"}}
                    norma, nueva = session.norma(**line)
                    if nueva:
                        table.add_row(
                            str(norma.id),
                            norma.titulo, str(True))
                    else:
                        table.add_row(
                            str(norma.id),
                            norma.titulo, str(False))
            console.print(table)
        else:
            print("La ruta al archivo es incorrecta")
    else:
        print("No has entregado la ruta al archivo")


if __name__ == '__main__':
    run()
