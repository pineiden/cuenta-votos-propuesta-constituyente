import ujson as json
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import (
    Column, Integer, 
    Text, String,
    ForeignKey,
    DateTime)
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func
from .base import Base
from sqlalchemy.schema import UniqueConstraint

class Norma(Base):
    __tablename__ = 'norma'
    #__table_args__ = {'schema': 'constitucion'}
    id = Column(Integer, primary_key=True)
    titulo = Column(String(300), unique=True, nullable=False)
    votos = relationship('Voto', backref='norma')
    
    def __hash__(self):
        return hash((self.id, self.titulo))
    
    def __str__(self):
        return f"{self.id} :: {self.titulo}"

    def __repr__(self):
        return f"Norma({self.id}, {self.titulo})"

    @property
    def json(self):
        return  {
            "id": self.id,
            "titulo": self.titulo
        }

class Voto(Base):
    __tablename__ = 'voto'
    __table_args__ = (
        UniqueConstraint('norma_id', 'dt_gen', name='medida_unica'),
    )
    # se deja esta constrain para monitorear modificaciones a la baja
    # de las votaciones
    id = Column(Integer, primary_key=True)    
    norma_id = Column(Integer, ForeignKey('norma.id'))
    dt_gen = Column(DateTime(timezone=True), server_default=func.now())
    cantidad = Column(Integer, unique=False)

    def __hash__(self):
        return hash((self.id, self.norma_id, self.cantidad))
    
    def __str__(self):
        return f"Norma {self.norma_id} :: {self.dt_gen.isoformat()} ::{self.cantidad}"

    def __repr__(self):
        return f"Voto(Norma {self.norma_id}, {self.dt_gen}, {self.cantidad})"



    @property
    def json(self):
        return  {
            "id": self.id,
            "norma_id": self.norma_id,
            "dt_gen": self.dt_gen,
            "cantidad": self.cantidad
        }
