from sqlalchemy import update
from sqlalchemy import Table, MetaData
from sqlalchemy.sql import text
from basic_logtools.filelog import LogFile
from pathlib import Path
from .models import (
    Norma,
    Voto)
import databases
from .db_session import DBSession

import datetime

from sqlalchemy.sql import select
from sqlalchemy.sql import table
from sqlalchemy.sql import column
from sqlalchemy import inspect
from sqlalchemy import and_, func
from dotenv import dotenv_values
from datetime import datetime
import pytz
from pytz import reference


def object_as_dict(obj):
    return {c.key: getattr(obj, c.key)
            for c in inspect(obj).mapper.column_attrs}


class SessionHandle(DBSession):
    """
    A session middleware class to manage the basic elements in the database,
    has generic methods to verify the elements existence, update_ and obtain
    lists from tables
    """

    def __init__(self, *args, is_async=False, **kwargs):
        super().__init__(*args, **kwargs)
        self.is_async = is_async
        self.metadata = MetaData()
        code = kwargs.get('code', 'ORM')
        path = Path(__file__).absolute().parent.parent / ".env"
        params = dotenv_values(path)
        LOG_PATH = params.get("LOG_PATH")
        log_path = kwargs.get('log_path', LOG_PATH)
        log_level = kwargs.get('log_level', 'INFO')
        hostname = kwargs.get('hostname', 'localhost')
        if not log_path:
            log_path = '~/log'
        print("Settting log path", log_path)

        Path(log_path).mkdir(parents=True, exist_ok=True)
        self.logger = LogFile(self.class_name,
                              code,
                              hostname,
                              path=log_path,
                              base_level=log_level)
        self.path = Path(__file__).parent

    @property
    def conn(self):
        return self.connection
        
    def commit(self, save=True):
        self.session.commit()

    @property
    def class_name(self):
        return self.__class__.__name__

    def close(self):
        self.session.close()

    def exists_table(self, table_name, **kwargs):
        """
        Check if table_name exists on schema
        :param table_name: a table_name string
        :return: boolean {True,False}
        """
        if bool(kwargs):
            this_schema = kwargs['schema']
        else:
            this_schema = 'collector'

        return self.engine.dialect.has_table(self.engine.connect(), table_name)

    def exists_field(self, table_name, field_name, **kwargs):
        """
        Check if field exist in table
        :param table_name: table name string
        :param field_name: field name string
        :return:  bolean {True, False}
        """
        if bool(kwargs):
            this_schema = kwargs['schema']
        else:
            this_schema = 'collector'

        assert self.exists_table(table_name), 'No existe esta tabla'
        fields = Table(
            table_name,
            self.metadata,
            autoload=True,
            autoload_with=self.engine,
            schema=this_schema)
        r = [c.name for c in fields.columns]
        try:
            r.remove('id')
        except ValueError as e:
            self.logger.exception("Error al checkear campo, %s" % e)
        assert field_name in r, 'No existe este campo en tabla'
        return True

    def value_type(self, table_name, field_name, value, **kwargs):
        """
        Check if value is the same value type in field
        :param table_name: table name string
        :param field_name: field name string
        :param value:  some value
        :return: boolean {True, False, None}; None if value type doesn't exist
        on that list, because there are only the most common types.
        """
        if bool(kwargs):
            this_schema = kwargs['schema']
        else:
            this_schema = 'collector'

        # get type value
        assert self.exists_table(table_name), 'No existe esta tabla'
        fields = Table(
            table_name,
            self.metadata,
            autoload=True,
            autoload_with=self.engine,
            schema=this_schema)
        r = [c.name for c in fields.columns]
        assert self.exists_field(table_name, field_name), \
            'Campo no existe en tabla'
        this_index = r.index(field_name)
        t = [str(c.type) for c in fields.columns]
        this_type = t[this_index]
        b = False
        if this_type == 'INTEGER' or this_type == 'BigInteger':
            assert isinstance(value, int)
            b = True
        elif this_type[0:7] == 'VARCHAR' or this_type == 'TEXT' or this_type == 'STRING':
            assert isinstance(value, str)
            b = True
        elif this_type == 'BOOLEAN':
            assert isinstance(value, bool)
            b = True
        elif this_type == 'DATE':
            assert isinstance(value, datetime.date)
            b = True
        elif this_type == 'DATETIME':
            assert isinstance(value, datetime.datetime)
            b = True
        elif this_type == 'FLOAT' or this_type == 'NUMERIC':
            assert isinstance(value, float)
            b = True
        else:
            b = None

        return b
        # check

        def generic_query(self, st):
            q = text(st)
            u = self.session.execute(q)
            return(u)

    def update_table(self, table_name, instance, field_name, value, **kwargs):
        """
        Change some value in table
        :param table_name: table name class
        :param instance: instance to modify in database
        :param field: field name string
        :param value: value
        :return: void()
        """
        this_schema = kwargs.get('schema', 'collector')
        # update to database
        table = Table(
            table_name,
            self.metadata,
            autoload=True,
            autoload_with=self.engine,
            schema=this_schema
        )
        up = update(table).where(
            table.c.id == instance.id).values({field_name: value})
        self.session.execute(up)
        self.session.commit()

    # LIST ELEMENTS

    def get_list(self, model):
        """
        Get the complete list of elements in some Model Class (Table in db)

        :param model:Model Class
        :return: a query list
        """
        return self.session.query(model).all()


# Class Alias
SH = SessionHandle

class SessionDB(SH):
    """
    An specific SessionHandler who extends database model in Collector case,
    has a Station, a Protocol and a FBData tables
    """

    # STATION TABLE
    normas = {}

    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)

    def connect(self):
        super().connect()
        self.get_normas()
        return self

    def norma(self, save=True, **kwargs):
        norma = Norma(**kwargs)
        existe = self.normas.get(norma.id)
        if not existe:
            norma, exists = self.create_norma(norma, save=save)
            return norma, exists
        else:
            return existe, False

    def create_norma(self, norma, save=True):
        try:
            self.session.add(norma)
            if save:
                self.session.commit()
            self.normas[norma.id] = norma
            return norma, True
        except Exception as err:
            self.session.rollback()
            return norma, False
        
    def update_norma(self, instance, new_dict):
        pass

    def delete_norma(self, norma):
        self.session.delete(norma)
        self.session.flush()

    def get_normas(self):
        normas = self.get_list(Norma)
        for norma in normas:
            self.normas[norma.id] = norma
        return normas 

    def get_new_normas(self, normas=[]):
        new_normas = self.session.query(Norma).filter(
                Norma.id.not_in(normas) 
        ).order_by(Norma.id.asc())
        for norma in new_normas:
            self.normas[norma.id] = norma
        return [n for n in new_normas]


    def get_norma(self, number):
        # code is a unique value
        norma = self.normas.get(number)
        if norma:
            return norma

    def voto(self, save=True,**kwargs):
        number = kwargs.get("norma_id")
        norma = self.get_norma(number)
        cantidad = int(str(kwargs.get("cantidad",0)).replace(".",""))
        kwargs["cantidad"] = cantidad
        voto = Voto(**kwargs)
        if norma:
            voto, new = self.create_voto(voto, save=save)
            return voto, new 
        else:
            return voto, False
   
    def create_voto(self, voto, save=True):
        try:
            self.session.add(voto)
            if save:
                self.session.commit()
            return voto, True
        except Exception as err:
            self.session.rollback()
            return voto, False

    def delete_voto(self, voto):
        self.session.delete(voto)
        self.session.flush()

    def get_votos(self, norma, dt_start, dt_end):
        queryset = sorted(self.session.query(Voto).filter(
            and_(
                Voto.dt_gen.between(dt_start, dt_end), 
                Voto.norma_id == norma.id)
        ), key=lambda e: e.dt_gen)
        votos = []
        tasa = 0
        delta = 0
        delta_votos = 0
        anterior = None
        tz = pytz.timezone("America/Santiago")
        for voto in queryset:
            data = voto.json
            dt = voto.dt_gen
            data["dt_gen"] = dt.astimezone(tz)
            if anterior:
                delta = voto.dt_gen - anterior.dt_gen 
                val_ant = int(str(anterior.cantidad).replace(".",""))
                val_voto = int(str(voto.cantidad).replace(".",""))
                delta_votos = (val_voto-val_ant) 
                tasa = ( delta_votos/ delta.seconds) * 60
                data.update({"delta":delta_votos, "tasa":tasa, "titulo": norma.titulo})
            else:
                data.update({"delta":delta, "tasa":tasa, "titulo":
                             norma.titulo})
            votos.append(data)
            anterior = voto

        return votos 


    def ultimo_voto(self, norma, end_date=None):
        if not end_date:
            end_date = datetime.utcnow()
        last_voto = self.session.query(Voto).filter(
            and_(
                Voto.dt_gen <= end_date, 
                Voto.norma_id == norma.id)
        ).order_by(Voto.id.desc()).first()
        if last_voto:
            return last_voto.json
        else: 
            return {}
