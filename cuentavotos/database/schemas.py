# pydantic vase models
from typing import Optional
from pydantic import BaseModel
from datetime import datetime

class NormaBase(BaseModel):
    titulo: str


class NormaCreate(NormaBase):
    pass


class Norma(NormaBase):
    id: int

    class Config:
        orm_mode = True


class VotoBase(BaseModel):
    dt_gen: datetime
    cantidad: int

class VotoCreate(VotoBase):
    pass


class Voto(VotoBase):
    id: int

    class Config:
        orm_mode = True
