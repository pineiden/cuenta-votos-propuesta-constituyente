"""
Prepara gráficas:

- serie tiempo evolucioń de 1 o mas propuestas
- opcional :: ls diferencia de tiempo
- opcional :: la tasa por minuto entre tramos

Fuentes de datos:
- Archivo CSV de datos (id, id_norma, titulo, dt_gen, cantidad, delta, tasa)
- Dataset consultado desde database
"""

import matplotlib.pyplot as plt
import numpy as np
import matplotlib.dates as mdates
import itertools
import csv
from pathlib import Path 
from datetime import datetime 
from rich import print
import matplotlib.ticker as ticker
import matplotlib.ticker as plticker
import pytz

class PlotNormas:
    """
    Gráficas en SerieTiempo
    """
    order = ["tasa", "delta", "simple"]
    dataset = {}
    limits = {}

    def __init__(self, normas, start, end, dataset, figsize=(20,15 ),
                 option={"tasa": True}, 
                 tz = pytz.timezone("America/Santiago"),
                 filename="data.pn"):
        self.pre_dataset = dataset
        self.figsize = figsize
        self.seleccion = normas
        if isinstance(start, str):
            start = datetime.fromisoformat(start).astimezone(tz)
        if isinstance(end, str):       
            end = datetime.fromisoformat(end).astimezone(tz)
        self.start = start
        self.end = end
        self.normas = normas
        self.filename = filename
        self.prepare()
        if self.dataset:
            for item in self.order:
                if option.get(item):
                    if item == "tasa":
                       self.fig, self.axes =  self.tasa()
                    elif item == "delta":
                       self.fig, self.axes =  self.delta()
                    elif item == "simple":
                       self.fig, self.axes =  self.simple()
                    for axis in self.axes:
                        axis.grid()
                    break

    def prepare(self):
        """
        agroup datasets by norma
        """
        item = self.pre_dataset.pop()
        
        
        for key, group in itertools.groupby(
                self.pre_dataset, lambda e: e.get("norma_id")):
            #for item in group:
            if key in self.normas:
                self.dataset[key] = list(filter(lambda e:
                                                e.get("dt_gen") >= self.start
                                                and
                                                e.get("dt_gen") <= self.end,group))
        del self.pre_dataset

    def show(self):
        print("Mostrando")
        self.fig.show()

    def save(self, filename="normas.png"):
        self.fig.savefig(filename)

    def draw(self, axis_set, delta=True, tasa=True):
        try:
            if tasa:
                cantidad, delta, tasa = axis_set
                for norma, dataset in self.dataset.items():
                    time_set = [item.get("dt_gen") for item in dataset]
                    cantidad_set = [item.get("cantidad") for item in dataset]
                    cantidad.plot(time_set, cantidad_set, label=norma)
                    cmin, cmax = (min(cantidad_set), max(cantidad_set))


                    delta_set = [item.get("delta") for item in dataset]
                    delta.bar(time_set, delta_set,
                              label=norma,align='center', width=0.05)
                    dmin, dmax = (min(delta_set), max(delta_set))

                    tasa_set = [item.get("tasa") for item in dataset]
                    tasa.plot(time_set, tasa_set, label=norma)
                    tmin, tmax = (min(tasa_set),
                                  max(tasa_set))
                    self.limits[norma] ={
                        "cantidad": (cmin,cmax),
                        "delta": (dmin, dmax),
                        "tasa":(tmin,tmax)
                    }

            elif delta:
                cantidad, delta = axis_set
                for norma, dataset in self.dataset.items():
                    time_set = [item.get("dt_gen") for item in dataset]
                    cantidad_set = [item.get("cantidad") for item in dataset]
                    cantidad.plot(time_set, cantidad_set, label=norma)
                    cmin, cmax = (min(cantidad_set), max(cantidad_set))

                    delta_set = [item.get("delta") for item in dataset]
                    delta.plot(time_set, delta_set, label=norma)
                    dmin, dmax = (min(delta_set), max(delta_set))

                    self.limits[norma] = {
                        "cantidad": (cmin,cmax),
                        "delta": (dmin, dmax),
                        "tasa":(tmin,tmax)
                    }

            else:
                cantidad = axis_set
                for norma, dataset in self.dataset.items():
                    time_set = [item.get("dt_gen") for item in dataset]
                    cantidad_set = [item.get("cantidad") for item in dataset]
                    cantidad.plot(time_set, cantidad_set, label=norma)
                    cmin, cmax = (min(cantidad_set), max(cantidad_set))
                self.limits[norma] = {
                    "cantidad": (cmin,cmax),
                }

        except Exception as e:
            print("Selección incorrecta de gráfica")

    def simple(self): 
        fig, axis = plt.subplots(1, sharex=True)
        fig.suptitle("Gráfico de Evolución de votos", fontsize=30)
        axis.set_xlabel("t", fontsize=14)
        axis.set_ylabel("votos", fontsize=14)
        axis.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
        axis.legend(bbox_to_anchor=(1, 0), loc="lower right",
                    bbox_transform=fig.transFigure, ncol=3)
        fig.autofmt_xdate()
        self.draw([axis], delta=False, tasa=False)
        return fig, (axis,)

    def delta(self):
        total = plt.subplots(
            2, 
            sharex=True, 
            figsize=self.figsize)
        fig, (axis_cantidad, axis_delta) = plt.subplots(
            2, 
            sharex=True)
        fig.suptitle("Gráfico de Evolución de votos con diferencias", fontsize=30)

        # axis_cantidad.set_xlabel("t", fontsize=14)
        axis_cantidad.set_ylabel("votos", fontsize=20)
        axis_delta.set_xlabel("t", fontsize=20)
        axis_delta.set_ylabel("diferencia", fontsize=20)

        axis_cantidad.xaxis.set_major_formatter(mdates.DateFormatter("%Y/%m/%dT%H:%M"))

        axis_cantidad.legend(bbox_to_anchor=(1, 0), loc="lower right",
                             bbox_transform=fig.transFigure, ncol=3)
        axis_delta.legend(bbox_to_anchor=(1, 0), loc="lower right",
                          bbox_transform=fig.transFigure, ncol=3)
        fig.autofmt_xdate()
        self.draw((axis_cantidad, axis_delta), tasa=False)
        return fig, (axis_cantidad, axis_delta)

    def tasa(self):
        print(self.figsize)
        fig, (axis_cantidad, axis_delta, axis_tasa) = plt.subplots(3,
                                                                   sharex=True)
        fig.set_figwidth(self.figsize[0])
        fig.set_figheight(self.figsize[1])

        fig.suptitle("Gráfico de Evolución de votos con tendencias", fontsize=30)

        axis_cantidad.set_ylabel("votos", fontsize=20)
        axis_delta.set_ylabel("diferencia", fontsize=20)
        axis_tasa.set_xlabel("tiempo", fontsize=20)
        axis_tasa.set_ylabel("tasa [votos/minuto]", fontsize=20)
        axis_cantidad.xaxis.set_major_formatter(mdates.DateFormatter("%Y/%m/%dT%H:%M"))
        axis_delta.xaxis.set_major_formatter(mdates.DateFormatter("%Y/%m/%dT%H:%M"))
        axis_tasa.xaxis.set_major_formatter(mdates.DateFormatter("%Y/%m/%dT%H:%M"))
        self.draw((axis_cantidad, axis_delta, axis_tasa))
        minc, maxc = self.min_max("cantidad") 
        step = (maxc - minc) / 10
        loc = ticker.MultipleLocator(step) # this locator puts ticks at
        axis_cantidad.yaxis.set_major_locator(loc)
        axis_cantidad.yaxis.set_major_formatter(ticker.FormatStrFormatter('%d'))
        loc = ticker.MultipleLocator(5) # this locator puts ticks at

        mind, maxd = self.min_max("delta") 
        step = (maxd - mind) / 10

        axis_delta.yaxis.set_major_locator(loc)
        axis_delta.yaxis.set_major_formatter(ticker.FormatStrFormatter('%d'))
        loc = ticker.MultipleLocator(1) # this locator puts ticks at

        mint, maxt = self.min_max("tasa") 
        step = (maxt - mint) / 10

        axis_tasa.yaxis.set_major_locator(loc)
        axis_tasa.yaxis.set_major_formatter(ticker.FormatStrFormatter('%0.1f'))

        axis_cantidad.tick_params(axis='both', which='major', labelsize=14)
        axis_cantidad.tick_params(axis='both', which='minor', labelsize=0)

        axis_delta.tick_params(axis='both', which='major', labelsize=14)
        axis_delta.tick_params(axis='both', which='minor', labelsize=0)

        axis_tasa.tick_params(axis='both', which='major', labelsize=14)
        axis_tasa.tick_params(axis='both', which='minor', labelsize=0)

        # axis_cantidad.legend(bbox_to_anchor=(1, 0), loc="lower right",
        #                      bbox_transform=fig.transFigure, ncol=3)
        # axis_delta.legend(bbox_to_anchor=(1, 0), loc="lower right",
        #                   bbox_transform=fig.transFigure, ncol=3)

        # axis_tasa.legend(bbox_to_anchor=(1, 0), loc="lower right",
        #                  bbox_transform=fig.transFigure, ncol=3)
        fig.autofmt_xdate()
        return fig, (axis_cantidad, axis_delta, axis_tasa)


    def min_max(self, field):
        minimun = []
        maximun = []
        for norma, values in self.limits.items():
            minimun.append(min(values.get(field)))
            maximun.append(max(values.get(field)))
        return min(minimun), max(maximun)


class PlotNormasCSVSource(PlotNormas):
    """
    Gráficas en SerieTiempo
    """

    def __init__(self, normas, start, end, source_csv, 
                 option={"tasa": True}, 
                 tz = pytz.timezone("America/Santiago")):
        self.normas = normas
        dataset = self.load_csv(source_csv)
        if isinstance(start, str):
            start = datetime.fromisoformat(start).astimezone(tz)
        if isinstance(end, str):       
            end = datetime.fromisoformat(end).astimezone(tz)
        super().__init__(normas, start, end, dataset, option=option, tz=tz)

    def load_csv(self, source_csv):
        if Path(source_csv).exists():
            dataset = []
            with open(source_csv, "r") as f:
                reader = csv.DictReader(f, delimiter=";",
                                        fieldnames=["id","norma_id",
                                                    "titulo","dt_gen","cantidad",
                                                    "delta","tasa"])
                for row in reader:
                    row["norma_id"] = int(row["norma_id"])
                    row["dt_gen"] = datetime.fromisoformat(row["dt_gen"])
                    if int(row["norma_id"]) in self.normas:
                        dataset.append(row)
                return dataset
        else:
            print("No existe el archivo")
            return []
