import aioredis
import asyncio
from fastapi import FastAPI
from cuentavotos.database.manager import SessionDB
from datetime import datetime
from typing import Optional
from fastapi.middleware.cors import CORSMiddleware
from fastapi import Depends,  HTTPException
from fastapi_cache import FastAPICache
from fastapi_cache.backends.redis import RedisBackend
from fastapi_cache.decorator import cache
from fastapi.responses import StreamingResponse

app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:8080",
    "http://localhost:6060",
    "http://localhost:3000",
    "http://127.0.0.1:8000",
    "http://165.232.132.187:8000"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@cache()
async def get_cache():
    return 1


# @app.on_event("startup")
# async def startup():
#     await session.connect()


# @app.on_event("shutdown")
# async def shutdown():
#     await session.disconnect()

# Dependency
def get_db():
    session = SessionDB()
    try:
        session.connect()
        yield session
    except Exception as e:
        print("Error al crear sesion",e)
    finally:
        session.close()

@app.get("/")
async def root():
    return {"message": "Hello World"}

import os
REDIS = os.environ.get(
    "REDIS_CACHE",
    "redis://localhost")


async def fake_video_streamer():
    for i in range(10):
        yield b"some fake video bytes"


@app.get("/do")
async def main():
    return StreamingResponse(fake_video_streamer())


@app.on_event("startup")
async def startup():
    redis =  aioredis.from_url(
        REDIS,
        encoding="utf8",
        decode_responses=True)
    FastAPICache.init(RedisBackend(redis), prefix="fastapi-cache")

import ujson as json

async def stream_normas(session):
    for nro, norma in session.normas.items():
        esta_norma = norma.json
        ultimo_voto = session.ultimo_voto(norma)
        esta_norma["ultimo_voto"] = ultimo_voto
        yield json.dumps(esta_norma, default= lambda e: e.isoformat())

@app.get("/stream_normas")
async def stream_all_normas(session: SessionDB = Depends(get_db)):
    return StreamingResponse(stream_normas(session))

@app.get("/normas")
@cache(expire=300)
async def all_normas(session: SessionDB = Depends(get_db)):

    print("Obteniendo normas", session)
    normas = []
    for key, norma in session.normas.items():
        esta_norma = norma.json
        ultimo_voto = session.ultimo_voto(norma)
        esta_norma["ultimo_voto"] = ultimo_voto
        normas.append(esta_norma)
    return sorted(
        normas, 
        key=lambda item: item.get("ultimo_voto",{}).get("cantidad",0))

@app.get("/votos/{norma_id}")
@cache(expire=300)
async def votos_norma(norma_id:int, 
                      start: str="2021-11-01T00:00:00",
                      end: str="2022-02-05T00:00:00",
                      session: SessionDB = Depends(get_db)):
    norma_instance = session.get_norma(norma_id)
    votos = []
    del_fields = ["norma_id", "titulo"]
    if norma_instance:
        start_date = datetime.fromisoformat(start)
        end_date = datetime.fromisoformat(end)
        votos = session.get_votos(norma_instance, start_date,
                                  end_date)
        for voto in votos:
            for field in del_fields:
                if field in voto:
                    del voto[field]
        return {"norma":norma_instance.json, 
                "start":start, 
                "end":end,
                "votos":votos}
    else:
        return []
