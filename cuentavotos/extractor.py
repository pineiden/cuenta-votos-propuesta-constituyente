import requests
from bs4 import BeautifulSoup
from rich import print
from rich.console import Console
from rich.table import Table

import asyncio 
from datetime import datetime
from tasktools.taskloop import TaskLoop
from slugify import slugify
import csv
from pathlib import Path 
import click
from .database.manager import SessionDB
from networktools.library import my_random_string
from enum import Enum
from functools import reduce


def scrapping(base_url, nro):
    url = f"{base_url}{nro}"
    result = requests.get(url)
    content = result.content
    html = BeautifulSoup(content, 'html.parser')
    css_id = "apoyo"
    result_A = html.find("div", {"id": css_id})
    if result_A:
        result = result_A.find("strong")
        if result:
            return result.contents[0]
        else:
            return None
    else:
        return None


class DBStep(Enum):
    CONNECT = 1
    DISTRUBUTE = 2
    EXECUTE = 3


import os
delta_time = int(os.environ.get("DELTA_TIME", 600))
if delta_time  < 300:
    delta_time = 300

fieldnames = ["fecha-hora", "votos"]

async def scrapping_loop(
        control, 
        idt, 
        base_url, 
        votos,
        registro, 
        console, 
        normas_groups, 
        files, 
        *args,
        **kwargs):
    await asyncio.sleep(delta_time*.1)
    usar_db = kwargs.get("usar_db")
    filename = kwargs.get("filename")
    for norma in normas_groups.get(idt, []):       
        norma_control = control.get(norma)
        if norma_control and not usar_db:
            now = datetime.utcnow()
            for norma in normas_groups.get(idt, []):
                title = norma.titulo
                nro = norma.id
                filename = registro / f"{slugify(title)}_{slugify(now.isoformat())}.csv" 
                files[nro] = filename
                kwargs["filename"] = filename
                control[norma] = False
                with open(filename, "a") as file_csv:
                    console.print(now, f"Inicializando norma {nro} archivo {filename}")
                    writer_csv  = csv.DictWriter(
                        file_csv, 
                        delimiter=";",
                        fieldnames=fieldnames,
                        quoting=csv.QUOTE_MINIMAL)
                    writer_csv.writeheader()
    # iterar sobre el las normas del grupo de esta tarea asignadas
    for norma in normas_groups.get(idt, []):        
        voto_anterior = votos.get(norma, 0)        
        nro = norma.id
        title = norma.titulo
        result = scrapping(base_url, nro)
        if result:
            now = datetime.utcnow()
            if result != voto_anterior:
                votos[norma] = result
                data = dict(
                    zip(fieldnames, [now, result]))
                csv_data = dict(
                     zip(fieldnames, [now.isoformat(), result]))
                if usar_db:
                    queue = kwargs.get("queue")                   
                    if queue:
                        await queue.put((nro, data))
                    else:
                        print(now, "No hay cola de envío, pero si se"+
                              "debe guardar a db")
                else:
                    fieldname = files.get(nro)
                    with open(filename, "a") as file_csv:
                        writer_csv  = csv.DictWriter(
                            file_csv,
                            delimiter=";",
                            fieldnames=fieldnames,
                            quoting=csv.QUOTE_MINIMAL)
                        writer_csv.writerow(csv_data)
                value = [now, nro, title, result]
                console.print(*value)
            else:
                console.print(now, f"En esta lectura para {title}, no hay nuevos votos")
    await asyncio.sleep(delta_time)
    return [control, idt, base_url, votos, registro, console,
            normas_groups, files], kwargs

def chunk(array, n):
    tasks = [set() for i in range(n)]
    index = 0
    while array:
        element = array.pop()
        if element:
            tasks[index].add(element)
        index += 1
        if index == len(tasks):
            index = 0
    return tasks


async def db_loop(control, session, queue, normas_groups, votos, **kwargs):
    if control == DBStep.CONNECT:
        try:
            session = SessionDB()
            control = DBStep.DISTRUBUTE
        except Exception as e:
            control = DBStep.DISTRUBUTE

    if control == DBStep.DISTRUBUTE:
        now = datetime.utcnow()
        for norma in session.normas.values():
            votos[norma] = session.ultimo_voto(norma).get("cantidad",0)
        normas = list(session.normas.values()) 
        # diccionariode agrupacion, cantidad de tareas
        groups = len(normas_groups.keys())
        # distribucion
        keys_groups = list(normas_groups.keys())
        distribucion = list(chunk(normas, groups))
        for  i, grupo_normas in enumerate(distribucion):
            if keys_groups:
                idt = keys_groups.pop()
                normas_groups[idt] |= grupo_normas
            else:
                keys_groups = list(normas_groups.keys())

        control = DBStep.EXECUTE
            
    if control == DBStep.EXECUTE:
        # get new normas
        # normas a repartir
        normas = list(session.normas.keys()) 
        # diccionariode agrupacion, cantidad de tareas
        groups = len(normas_groups.keys())
        # distribucion
        new_normas = []
        now = datetime.utcnow()
        try:            
            new_normas = session.get_new_normas(normas)
        except Exception as e:
            control = DBStep.CONNECT
       
        while new_normas:
            groups = len(normas_groups.keys())
            distribucion = list(chunk(new_normas, groups))
            keys_groups = list(normas_groups.keys())
            for  i, grupo_normas in enumerate(distribucion):
                if keys_groups:
                    idt = keys_groups.pop()
                    normas_groups[idt] |= grupo_normas
                else:
                    keys_groups = list(normas_groups.keys())

            normas = list(session.normas.keys()) 
            try:
                new_normas = session.get_new_normas(normas)
            except Exception as e:
                control = DBStep.CONNECT
        if not queue.empty():
            for i in range(queue.qsize()):
                nro, voto = await queue.get()
                data = {
                    "norma_id": nro,
                    "dt_gen": voto.get("fecha-hora"),
                    "cantidad": voto.get("votos")
                }
                new_voto, nuevo = session.voto(**data)
                if nuevo:
                    print(new_voto.dt_gen, 
                          f"ID voto {new_voto.id}",
                          f"norma {nro}", 
                          "Guardado en db", new_voto.cantidad)
                else:
                    print(voto.get("fecha-hora"),
                          voto.get("cantidad"),  
                          "No almacenado en db, ya existe")
        else:
            print(datetime.utcnow(), datetime.now(), "Cola vacía")
    # sleep por 2 min
    await asyncio.sleep(delta_time*.9)
    return (control, session, queue, normas_groups, votos), kwargs


def read_csv(filename):
    path = Path(filename)
    ids = []
    if path.exists():
        with open(path, "r") as f:
            reader = csv.DictReader(f, delimiter=";")
            for row in reader:
                ids.append(row)
    return ids

def read_msg():
    console = Console()
    path = Path(__file__).absolute().parent
    table = Table(title="Cuentavotos Normas Constituyentes Chile")
    table.add_column("Información del Proyecto Libre", style="blue")
    with open(path / "mensaje","r") as f:
        lines = []
        for line in f.readlines():
            table.add_row(line.strip())
        console.print(table)


def main_tasks(base_url, registro, normas_groups, votos, tasks=[],  **opts):
    loop = asyncio.get_event_loop()
    for idt in normas_groups.keys():
        console = Console()
        files = {}
        control = {}
        args = [control, idt, base_url, votos, registro, console,
                normas_groups, files]
        task = TaskLoop(scrapping_loop, coro_args=args, coro_kwargs=opts)
        task.create()
        tasks.append(tasks)

    loop.run_forever()



@click.command()
@click.option("--propuestas", 
              help="ruta a archivo csv con dos columnas: id, título", 
              required=False)
@click.option("--usar-db/--not-usar-db", 
              help="ruta a archivo csv con dos columnas: id, título", 
              required=False)
@click.option("--cantidad_tareas", default=12,
              help="cantidad de tareas asíncronas", 
              required=False)
def run(propuestas, usar_db, cantidad_tareas):
    read_msg()
    registro = Path(__file__).absolute().parent / "csv"
    if not registro.exists():
        registro.mkdir()
    main_console = Console()
    do = False
    # dadas la cantidad de tareas, asignar un elemento set.
    # asigna id a cada tarea, y un set para agrupar las normas
    normas_groups = {my_random_string():set()  for i in
                     range(cantidad_tareas)}
    now = datetime.utcnow()
    print(now, f"Tareas {cantidad_tareas}", "Asignadas", len(normas_groups.keys()))
    ids = {}
    tasks = []
    base_url = "https://plataforma.chileconvencion.cl/m/iniciativa_popular/detalle?id="
    opts = {}
    votos = {}
    if propuestas:
        if Path(propuestas).exists():
            ids = read_csv(propuestas)
            
            # ids = [{
            #     43014:"Derecho a la privacidad en Internet"}, 
            #     {46114:"Derecho al conocimiento"}
            # ]
            sizes = {idn: len(values) for idn, values in
                     normas_groups.items()}
            listsizes = list(sizes.values())
            distribucion = list(chunk(ids, cantidad_tareas))
            for new_set in distribucion:
                # iteracion
                minsize = listsizes.pop(0)
                # toma el ide de esta tamaño
                to_assign = list(filter(lambda e: sizes[e] == minsize,
                                        sizes.keys()))     
                normas_groups[to_assign].union(new_set)
            
            do = True
        else:
            main_console.print("Debes entregar una ruta al csv con las propuestas que exista", style="red on white")
    elif usar_db:
        queue = asyncio.Queue()
        control = DBStep.CONNECT
        session = None
        args = [control, session, queue, normas_groups, votos]
        task_db = TaskLoop(db_loop, coro_args=args)
        task_db.create()
        tasks.append(task_db)        
        opts["usar_db"] = usar_db
        opts["queue"] = queue
        do = True
    else:
        main_console.print("Debes entregar una ruta al csv con las propuestas", style="red on white")
    if do:
        #(base_url, registro, normas_groups, tasks=[],
        main_tasks(base_url, 
                   registro, 
                   normas_groups,
                   votos,
                   tasks, 
                    **opts)
