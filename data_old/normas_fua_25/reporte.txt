La norma 0 no existe o ya fue registrada
La norma 5602 no existe o ya fue registrada
                                                                 Votos para norma Reporte para normas 25 de enero, diferencia 1 dias                                                                  
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┳━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━┳━━━━━━━━━━━━┓
┃ norma id ┃ titulo                                                                                                                     ┃ id    ┃ fecha-hora                 ┃ cantidad ┃ diferencia ┃
┡━━━━━━━━━━╇━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━╇━━━━━━━╇━━━━━━━━━━━━━━━━━━━━━━━━━━━━╇━━━━━━━━━━╇━━━━━━━━━━━━┩
│ 49134    │ La Soberanía En Una Democracia Plena Y Mecanismos De Participación Ciudadana Vinculante Como Garantía Constitucional       │ 31044 │ 2022-01-25T23:59:53.195394 │ 130      │ 11         │
│ 5602     │ Nacionalización De Las Empresas De La Gran Minería Del Cobre, Del Litio Y Del Oro                                          │ 31233 │ 2022-01-26T00:43:58.007619 │ 19557    │ 294        │
│ 30230    │ Por El Agua, Los Derechos De La Naturaleza Y Los Glaciares                                                                 │ 31185 │ 2022-01-26T00:32:56.141535 │ 19       │ 3          │
│ 5614     │ Propuesta Constitucional De Los Encuentros De "Trabajadoras Y Trabajadores Por Un Nuevo Chile"                             │ 31236 │ 2022-01-26T00:43:58.596945 │ 4870     │ 116        │
│ 17046    │ Derecho A La Alimentación, Un Derecho Fundamental E Inalienable De Los Pueblos                                             │ 31207 │ 2022-01-26T00:43:47.760879 │ 3995     │ 250        │
│ 17274    │ Democracia Directa                                                                                                         │ 31073 │ 2022-01-26T00:10:46.818168 │ 270      │ 10         │
│ 50610    │ Mecanismos De Democracia Directa Para Una República Plebeya                                                                │ 31050 │ 2022-01-25T23:59:54.451982 │ 242      │ 17         │
│ 33622    │ Soberanía de los pueblos en Tratados de Libre Comercio                                                                     │ 31200 │ 2022-01-26T00:43:46.520486 │ 2743     │ 349        │
│ 17550    │ Derecho Al Trabajo Garantizado Por El Estado                                                                               │ 30983 │ 2022-01-25T23:48:45.229077 │ 61       │ 6          │
│ 49090    │ Derecho A Un Transporte Público Digno, Seguro Y Frecuente, Día Y Noche, Para Todas Las Personas Sin Exclusión              │ 31225 │ 2022-01-26T00:43:56.442653 │ 262      │ 28         │
│ 41354    │ Refundación de Carabineros                                                                                                 │ 31214 │ 2022-01-26T00:43:54.172132 │ 783      │ 108        │
│ 47286    │ Fuerzas Armadas Al Servicio De Los Derechos Humanos Y La Democracia                                                        │ 31197 │ 2022-01-26T00:43:46.167224 │ 217      │ 47         │
│ 9026     │ Sistema Único De Salud, Universal, Plurinacional E Integrado Para El Nuevo Chile Que Estamos Construyendo Democráticamente │ 31208 │ 2022-01-26T00:43:47.883577 │ 15048    │ 1760       │
│ 47330    │ Para Que Nunca Más En Chile                                                                                                │ 31155 │ 2022-01-26T00:32:46.454875 │ 68       │ 14         │
│ 47754    │ ¡Aquí Manda El Pueblo! Soberanía Popular A La Nueva Constitución                                                           │ 31156 │ 2022-01-26T00:32:46.819870 │ 21       │ 4          │
│ 48182    │ No Más Odio, Fake News Y Negacionismo: A Defender La Democracia                                                            │ 31162 │ 2022-01-26T00:32:47.846153 │ 54       │ 13         │
│ 48806    │ Educación Social Integral: Por Un Nuevo Chile                                                                              │ 31167 │ 2022-01-26T00:32:49.070217 │ 27       │ 9          │
│ 42510    │ Formación Profesional En Oficios Desde La Enseñanza Media Y Con Certificación Mineduc                                      │ 31077 │ 2022-01-26T00:10:47.826567 │ 550      │ 52         │
│ 48858    │ Mecanismos de democracia directa                                                                                           │ 30453 │ 2022-01-25T21:36:35.279761 │ 20       │ 1          │
│ 2578     │ Derechos Laborales En La Constitución                                                                                      │ 31193 │ 2022-01-26T00:32:57.742161 │ 1147     │ 133        │
│ 18494    │ Con Nosotras También: Por El Reconocimiento De Los Derechos De Las Personas Chilenas En El Exterior                        │ 31169 │ 2022-01-26T00:32:53.086335 │ 537      │ 20         │
│ 6414     │ Gestión Y Desarrollo De Los Bienes Comunes Mineros                                                                         │ 30486 │ 2022-01-25T21:36:39.324045 │ 56       │ 9          │
│ 57318    │ Deber Del Estado De Protección, Justicia, Reparación Y Garantía De Inviolabilidad A Los Dd.Hh. En Todo Tiempo Y            │ 31209 │ 2022-01-26T00:43:53.096367 │ 711      │ 118        │
│          │ Circunstancia.                                                                                                             │       │                            │          │            │
│ 41126    │ Pobladoras y pobladores por el derecho  a la viviendo digna                                                                │ 31211 │ 2022-01-26T00:43:53.419664 │ 18388    │ 411        │
└──────────┴────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┴───────┴────────────────────────────┴──────────┴────────────┘
