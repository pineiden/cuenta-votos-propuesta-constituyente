id;titulo
49134;La Soberanía En Una Democracia Plena Y Mecanismos De Participación Ciudadana Vinculante Como Garantía Constitucional
5602;Nacionalización De Las Empresas De La Gran Minería Del Cobre, Del Litio Y Del Oro
30230;Por El Agua, Los Derechos De La Naturaleza Y Los Glaciares
5614;"Propuesta Constitucional De Los Encuentros De ""Trabajadoras Y Trabajadores Por Un Nuevo Chile"""
17046;Derecho A La Alimentación, Un Derecho Fundamental E Inalienable De Los Pueblos
17274;Derecho Político Colectivo De Participación Popular: Red De Cabildos Autónomos Deliberativos Vinculantes 
50610;Mecanismos De Democracia Directa Para Una República Plebeya
33622;¡Aseguremos La Soberanía De Los Pueblos En Los Tratados De Libre Comercio!
17550;Derecho Al Trabajo Garantizado Por El Estado
49090;Derecho A Un Transporte Público Digno, Seguro Y Frecuente, Día Y Noche, Para Todas Las Personas Sin Exclusión
41354;Propuesta De Refundación De Carabineros
47286;Fuerzas Armadas Al Servicio De Los Derechos Humanos Y La Democracia
9026;Sistema Único De Salud, Universal, Plurinacional E Integrado Para El Nuevo Chile Que Estamos Construyendo Democráticamente
47330;Para Que Nunca Más En Chile
47754;¡Aquí Manda El Pueblo! Soberanía Popular A La Nueva Constitución
48182;No Más Odio, Fake News Y Negacionismo: A Defender La Democracia
48806;Educación Social Integral: Por Un Nuevo Chile
42510;Formación Profesional En Oficios Desde La Enseñanza Media Y Con Certificación Mineduc
48858;Consejos Comunales Y Territoriales Para Que Los Pueblos Manden
2578;Derechos Laborales En La Constitución
18494;Con Nosotras También: Por El Reconocimiento De Los Derechos De Las Personas Chilenas En El Exterior
6414;Gestión Y Desarrollo De Los Bienes Comunes Mineros
57318;Deber Del Estado De Protección, Justicia, Reparación Y Garantía De Inviolabilidad A Los Dd.Hh. En Todo Tiempo Y Circunstancia.
5602;Nacionalización De Las Empresas De La Gran Minería Del Cobre, Del Litio Y Del Oro
41126;Pobladoras Y Pobladores Por El Derecho A La Vivienda Digna
7738;"Software Libre, para una formación cultural informática integral y autónoma"
