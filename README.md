Normas Populares Constituyente
==============================

En Chile (sudamerica) se está realizando un proceso consitituyente que
definirá el documento base que gobernará nuestro futuro. 

Se logró esta instancia en base a las movilizaciones que tuvieron su
culmine en octubre 2019. 

En conjunto con el trabajo de las diferentes comisiones, existe la
posibilidad de presentar propuestas de normas y que sean votadas. Cada
persona tiene a su disposición 7 votos y puede escoger libremente de
entre todas utilizando su registro único digital (clave única).

Las normas propuestas se pueden revisar aquí
[Normas](https://iniciativas.chileconvencion.cl) y esta herramienta
está diseñada para monitorear la evolución del voto en las que
consideres de tu interés.

Este software se puede ver en la práctica, operando como el backend de
la web [Votos](https://votos.eradelainformacion.cl/)

Monitorea tu Propuesta de Norma Constituyente
=============================================

Este programa libre te permitirá ir recogiendo los nuevos votos cada
un tanto de tiempo que gustes, por defecto lo tengo cada 10 minutos y,
pienso, es suficiente para el caso. No recomiendo bajar de eso ya que
saturaría la web a la que todos tienen derecho a acceder.

Para usarlo necesitas:

- python 3.8

Instalar el módulo:

```
pip install .
```

En modo desarrollo:


```
pip install -e .
```


Generación de archivo CSV:

Los valores importantes que pueden cambiar son el número id de la url
que aparece al final (después del =) y el título de la
iniciativa. Estos valores deberás guardarlos en un archivo de texto
plano *csv*.

Para correr el programa:

```
cuentavotos --propuestas propuestas.csv
```
Si tienes habilitada la *database* como se ve más adelante con *sqlite*
podrás guardar directamente en ella al recibir nuevos valores.

```shell
cuentavotos --usar-db
```

Te irá mostrando las nuevas votaciones cada tanto tiempo y estas se
guardarán en un archivo de texto csv o bien, en la database.

Si activas una, la otra no se habilita.

El sistema con una distribución de tareas a realizar, a cada una de
estas se le asigna un grupo de normas a monitorear. Si se agrega a la
*database*, en caso de usarla, entonces las nuevas normas se agregarán
a una de las tareas disponibles en su propia cola. 


Apoya a la comunidad de Software Libre y Cultura Libre
=======================================================

Somos una agrupación de ciudadanas/os que participan en la promoción
de las tecnologías libres, el conocimiento y la cultura libre. 

También estamos participando con propuestas de normas populares y te
pedimos tu apoyo:

- Privacidad en internet 
- Derecho al conocimiento 
- Soberanía tecnológica 

Revisa nuestras propuestas en la web https://www.eradelainformacion.cl/


Prepara tus propuestas 
----------------------

Basta con que crees un archivo con tus propuestas de tipo csv,
separado por ";" y luego lo completas con el id y título de interés.

```csv
id;titulo
43014;"Derecho a la privacidad en Internet"
46114;"Derecho al conocimiento"

```

De esta manera, si llamas al archivo "propuestas.csv", podrás
monitorear todas las propuestas que estén listadas en el archivo.

```
cuentavotos --propuestas propuestas.csv
```

Recuerda, si usas la database puedes cargar esta lista de propuestas y
nuevas propuestas así:

```shell
cargar_normas --normas propuestas.csv
```

y correr al programa.

```shell
cuentavotos --usar-db
```

Comandos disponibles
==================== 

Luego de instalar este *software*, podrás monitorear pero también
gestionar los datos. Una forma recomendada es pasar los datos
guardados en archivos *csv* a una *base de datos*.

La opción de utilizar una base de datos sencilla está disponible desde
esta versión. 

Crear database 
---------------

Para usar esta característica debes instalar *sqlite*. Si usas el
*modo web* no te servirá *sqlite* y debes utilizar *maríadb* (razones técnicas).

En windows [bajar sqlite](https://www.sqlite.org/download.html)

En GnuLinx lo instalas desde repositorios.

```
sudo apt install sqlite3 
sudo apt install sqlite3-dev
```

Se utiliza *sqlite3* para mantener de manera persistentes los datos y
necesitas crearla antes de proceder.

```shell
crear_db
```

Cargar normas
-------------

Una vez que ya tengas tu archivo de *propuestas.csv* puedes cargarlas
en la database, si ya existe no la carga. Como resultado muestra una
tabla con el resumen de la operación.

```shell
cargar_normas  --normas propuestas.csv 
```

Listar normas
-------------

Para visualizar todas las normas disponibles basta con escribir el
comando 

```shell
listar_normas
```

Cargar Votos 
------------

Para cargar votos desde un directorio con archivos *csv* que se
almacenan al monitorear las propuestas, se realiza lo siguiente.

```shell 
cargar_votos --base_path RUTA_CSV
```
En que el valor de la RUTA_CSV es relativa a la posición o absoluta,
se listaran las normas por la sesión a database y se inspeccionan los
archivos correspondiente a cada norma.

Como resultado muestra una tabla resumen por normas y archivos de
votos, además con la opción *--load* podraś cargar los datos de los
archvios en la *base de datos* 

```shell
cargar_votos --base_path RUTA_CSV --load
```

O bien, si se dispone de un *dump* (cuentavotos.sql) de una base de datos anterior, sería-

```shell
mariadb -b cuentavotos -u cuentavotos -p < cuentavotos.sql
```

Mostrar votos
-------------

Para mostrar los votos hay que considerar los siguientes paráemtros:

- ID de la norma 
- fecha de partida (start), por defecto 1 de noviembre 2021
- fecha final (end), por defecto 1 de febrero 2022

El formato de las fechas consideran las siguientes posibilidades:

- AÑO/MES/DIA 
- AÑO/MES/DIATHORA:MINUTO:SEGUNDO

Siendo posible, por ejemplo:

```shell
listar_votos --normas 6602 --start "2021-11-01" --end "2022-02-01"

```

O bien, con precisión al tiempo

```shell
listar_votos --normas 6602 --start "2021-11-01T15:00:00" --end "2022-02-01T01:30:20"

```

También, se pueden listar varias normas en conjunto, mostrando el
resumen en tabla. 

```shell
listar_votos -n 41126 -n 43014 --start "2021-11-01T15:00:00" --end "2022-02-01"
```

Guardar Votos en archivo CSV
----------------------------

Si necesitas guardar los datos en un archivo de texto ordenado en
formato *csv* puedes habilitar la opción de *--guardar*.


```shell
listar_votos -n 41126 -n 43014 --start "2021-11-01T15:00:00" --end
"2022-02-01" --guardar
```

Si necesitas definir la ruta y nombre del archivo.


```shell
listar_votos -n 41126 -n 43014 --start "2021-11-01T15:00:00" --end
"2022-02-01" --guardar --filename datos_votos.csv
```

El separador de campo es ";" (punto y coma)


Graficar votaciones y normas.
------------------------------

Asimismo como puedes guardar los datos en un archivo o consultarlos
directamente, puedes visualizarlos en un gráfico.


Desde un archivo csv *"datos.csv"*, puedes seleccionar las normas y el
intervalor de tiempo entregando las fechas en formato iso8601. (año-mes-diaThora:minuto:segundo)

```shell
graficar -n 41126 -n 43014 --start "2021-11-01T15:00:00" --end
"2022-02-01" --csv --path datos.csv 
```

O bien, consultar desde la base de datos.


```shell
graficar -n 41126 -n 43014 --start "2021-11-01T15:00:00" --end
"2022-02-01" 
```

Como se ve, puedes visualizar el grupo de normas que necesitas
añadiendo *"-m ID_NORMA"*

Crear reporte de votos de grupo de normas.
------------------------------------------

Este comando te permitirá crear un reporte del último voto por cada
norma que te intere generar un reporte, entrega una tabla en la
terminal y también, si necesitas, te permite guardar a un archivo
*csv*.

Crear reporte de normas, entregando id.

```shell 
ultimo_voto -n 45658 -n 15150 
```
Crear reporte de normas que estén listadas en un *csv* (id,
título). Puede ser el mismo archivo con que cargas las normas a la
base de datos.

Puedes usar la opción *--grupo* o bien en corto  *-g*.

```shell 
ultimo_voto -g lista_normas.csv 
```

Crear reporte combinado, con normas de tu elección directa y desde un
archivo *csv*.

```shell 
ultimo_voto -n 45658 -n 15150 -g lista_normas.csv 
```
Guardar este reporte de tablae en un archivo *txt*.

```shell 
ultimo_voto -n 45658 -n 15150 -g lista_normas.csv > reporte_ultimos_votos.txt
```
Guardar reporte a un archivo *csv*. Deberás entregar las opciones
*--guardar* y el nombre del archivo con *--filename*, de otra manera
se guardará por defecto como *reporte.csv*.

```shell
ultimo_voto -n 45658 -n 15150 -g lista_normas.csv  --guardar --filename reporte_votos.csv
```

Tambien, puede darse el caso de requerir la cantidad de votos a cierta
fecha y la diferencia de n (1 o mas) días desde entonces al pasado.

```shell 
ultimo_voto -g lista_normas.csv -t 2022-01-22 -d 4

```

Esto genera un reporte de salida estandar desde la fecha 22 de enero y
cuatro dias atrás. Se puede combinar tambień con las otras opciones.

Operar Sofware Cuentavotos en WEB 
================================

Si necesitas servir tu monitoreo de normas de manera pública, mediante
web considera lo siguiente 

- API REST 
- Base de datos como mysql/postgres 
- Un frontend

API REST 
-------

Para servir los datos recolectados mediante una API REST es tan
sencilo como utilizar la API que sirve el módulo de
*cuentavotos/api.py*.

Se recomienda usa *[uvicorn](https://www.uvicorn.org/)* para tales efectos y mantener un proxy con
*[nginx](https://www.nginx.com/)*.

Desde el directorio del proyecto, es suficiente escribir el comando.

```shell
uvicorn cuentavotos.api:app --reload

```
Luego, si necesitas alguna configuración extra deberás revisar el
manual, por ejemplo para disponer la conexión la red, tendraś que
poner.

```shell
uvicorn cuentavotos.api:app --host 0.0.0.0 --reload 

```
Y asi para configuración en producción, tendrá que ser más específico.

Frontend  
--------

El frontend, o interfaz de usuario, permitirá al público acceder a los
datos de manera visual y amigable.

Nosotrs tenemos la aplicación hecha en *Vue 3* y la biblioteca de
gráficas *D3*. Puedes acceder y montarla a gusto en [Frontend Cuentavotos](https://gitlab.com/pineiden/frontend-cuentavotos)

Base de datos
------------

Primero, una database que sirva a muchos usuarios cliente necesitará accesos
restringidos del servicio. Primero creamos la contraseña para la database.

```shell
PASSWORD="PasswordSegura!"
```

En modo web (producción):

Si deseas correr la versión web en un ambiente de producción es preciso que lo
hagas con una base de datos que lo soporte. En este caso usaremos MariaDB.

Crea una base de datos y credenciales para acceder a MySQL/MariaDB, reemplazar
*<PASSWORD>* por el valor asignado arriba (que sea diferente al que dice ahí).

```mysql
CREATE DATABASE IF NOT EXISTS cuentavotos CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci';
GRANT ALL PRIVILEGES ON cuentavotos.* TO 'cuentavotos'@'localhost' IDENTIFIED BY '<PASSWORD>';
FLUSH PRIVILEGES;
```

Ahora, haz una revisión si puedes acceder a la base de datos mediante la línea
de comandos. Debes ingresar la password creada.

```shell
mysql -u cuentavotos -p
```

Dale a conocer a tu consola la conexión a base de datos que usarás.
```sh
export DATABASE_URL='mysql+pymysql://cuentavotos:${PASSWORD}@localhost/cuentavotos?charset=utf8mb4'
```
Ojo: La URL tiene que tener un formato rfc1738 válido, por lo que la contraseña
debe ir codificada apropiadamente, puedes buscar un codificador en línea
buscando "url encoder", o usar una contraseña relativamente sencilla, si estás
conectando a localhost sin los puertos abiertos esto es bastante seguro.

(re)Genera el archivo `.env` con esta base de datos e instala dependencias.
```sh
python3 setup.py install
```

Instala PyMySQL
```sh
pip install PyMySQL
```

Cache para ahorrar capacidad de trabajo
======================

Con el fin de evitar realizar consultas a la base de datos de manera reiterada,
se almacenará en *cache* la información durante 5 minutos y luego se volverá a
refrescar ya que es posible que tenga nueva información adicional.

```shell
pip install "fastapi-cache2[redis]"
```

Ver la implementación aquí [Fast Api Cache](https://github.com/long2ice/fastapi-cache)

Cambio en la tasa de muestreo
=============================

Puedes cambiar la tasa de muestreo de definiendo el parámetro de
ambiente 

```
export DELTA_TIME=600
``` 

En cantidad de segundos, esta sera apróximada ya que las consultas son
asíncronas.

Nuevamente, repito, no es recomendable reducir de diez minutos por
temas de servicio y rendimiento de la web y porque NO es significativo
el resultado si disminuye la tasa, a lo menos 300 segundos.


Por hacer
=========

Consultas a DB asíncronas con módulo *databases*










